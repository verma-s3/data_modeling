# Normalising a table to BCNF

employee(name, email, dept, dept_mgr, dept_mgr,email)

PK = [pname,email,dept]
# This is in 1NF

------------------------------------------------------------------------------
#is this in 2NF..
# Answer: no, dept manager can be determined by alone, so we have a partila dependeency

employee([name, email, dept]) -- now is 2nf atleast
dept([dept], dept_mgr, dept_mgr_email) -- now in 2NF

-------------------------------------------------------------------------------

#Are both tables in 3NF
employee([name, email, dept]) -- is in 3NF
dept([dept], dept_mgr, dept_mgr_email) -- is not in 3NF as there is transitive dependency

-------------------------------------------------------------------------------

employee([name, email, dept]) -- is in 3NF
dept([dept], dept_mgr) -- is in 3NF
dept_mgr(dept_mgr, dept_mgr_email) -- is in 3NF 

# BCNF means no multivalue 

------------------------------------using related tables -----------------------------------------------------------

describe book;

SELECT 
book.book_id, book.title,
book.num_pages, book.year_published,
book.in_print,
book.image, book.price
FROM 
book;

-- two types of joins
# 1. Explicit Joins
# 2. Implicit Joins

SELECT 
book.book_id, book.title,
book.num_pages, book.year_published,
book.in_print, book.image, book.price,
author.name, author.country
FROM 
book
JOIN author ON(book.author_id = author.author_id);

SELECT 
book.book_id, book.title,
book.num_pages, book.year_published,
book.in_print, book.image, book.price,
author.name as author, author.country as author_country,
publisher.name as publisher, publisher.city as publisher_city
FROM 
book
JOIN author ON(book.author_id = author.author_id)
JOIN publisher ON(book.publisher_id = publisher.publisher_id);


SELECT 
book.book_id, book.title,
book.num_pages, book.year_published,
book.in_print, book.image, book.price,
author.name as author, author.country as author_country,
publisher.name as publisher, publisher.city as publisher_city,
genre.name as genre, format.name as format
FROM 
book
JOIN author ON(book.author_id = author.author_id)
JOIN publisher ON(book.publisher_id = publisher.publisher_id)
JOIN genre ON(book.genre_id = genre.genre_id)
JOIN format ON(book.format_id = format.format_id);

-- this is a specific kind of join: inner join 
-- returns he elemnet COMMON to both tables being joined

# *********************************************** PRACTICE *******************************************************
-- question 1
-- GEt list of books(title, author, genre, publisher, format)
-- order by title in alphabetical order

SELECT 
book.title,
author.name as author,
publisher.name as publisher,
genre.name as genre,
format.name as format
FROM 
book
JOIN author ON(book.author_id = author.author_id)
JOIN publisher ON(book.publisher_id = publisher.publisher_id)
JOIN genre ON(book.genre_id = genre.genre_id)
JOIN format ON(book.format_id = format.format_id)
ORDER BY title;

+---------------------+------------------+------------------+------------+-------------+
| title               | author           | publisher        | genre      | format      |
+---------------------+------------------+------------------+------------+-------------+
| A Day in the Life   | Carmen Ynez      | Penguin Books    | Literature | Hardcover   |
| A Mixed Blessing    | Sally Unger      | Sun Press        | Politics   | Trade Paper |
| Carpet Baggers      | Lee Sheldon      | Putnam           | Drama      | Paper       |
| Carrie              | Stephen King     | Ballantine Books | Horror     | Paper       |
| Castle of Adventure | Enid Blyton      | Delacorte        | SF         | Hardcover   |
| Caves of Steel      | Isaac Asimov     | Delacorte        | SF         | Hardcover   |
| Dune                | Frank Herbert    | Ballantine Books | SF         | Paper       |
| Dune Messiah        | Frank Herbert    | Ballantine Books | SF         | Paper       |
| Flash Forward       | Robert Sawyer    | DAW              | SF         | Hardcover   |
| Island              | Richard Laymon   | Dell             | Horror     | Paper       |
| Not a Penny More    | Daniel Chambers  | Delacorte        | Politics   | Paper       |
| The Black Box       | Michael Connelly | Delacorte        | Literature | Hardcover   |
| The Oath            | John Lescroart   | Dell             | Legal      | Hardcover   |
| Under the Dome      | Stephen King     | Ballantine Books | Horror     | Trade Paper |
+---------------------+------------------+------------------+------------+-------------+

-- Question 2
-- get a list o fbooks where year_published between 1978 and 2000
-- inclusive
-- title, author, publisher, format, year_published

SELECT 
book.title, book.year_published,
author.name as author,
publisher.name as publisher,
format.name as format
FROM 
book
JOIN author ON(book.author_id = author.author_id)
JOIN publisher ON(book.publisher_id = publisher.publisher_id)
JOIN format ON(book.format_id = format.format_id)
where year_published BETWEEN 1978 and 2000;

+------------------+----------------+-----------------+-----------+--------+
| title            | year_published | author          | publisher | format |
+------------------+----------------+-----------------+-----------+--------+
| Not a Penny More |           1980 | Daniel Chambers | Delacorte | Paper  |
+------------------+----------------+-----------------+-----------+--------+

-- Question 3
-- get a list of books where author has the letters 'in' anywhere
-- in the autor name
-- title, author, genre, format

SELECT 
book.title,
author.name as author,
genre.name as genre,
format.name as format
FROM 
book
JOIN author ON(book.author_id = author.author_id)
JOIN genre ON(book.genre_id = genre.genre_id)
JOIN format ON(book.format_id = format.format_id)
WHERE author.name like '%in%';

+----------------+--------------+--------+-------------+
| title          | author       | genre  | format      |
+----------------+--------------+--------+-------------+
| Carrie         | Stephen King | Horror | Paper       |
| Under the Dome | Stephen King | Horror | Trade Paper |
+----------------+--------------+--------+-------------+

-- Question 4 (require grouping and agreeate functions)
-- Get a list of publishers and the numbers of books for each 
-- publisher in the database
-- publisher, num_books

-- what we just did were expicit JOINS using the join 
SELECT 
book.book_id, book.title,
book.num_pages, book.year_published,
book.in_print, book.image, book.price,
author.name as author, author.country as author_country,
publisher.name as publisher, publisher.city as publisher_city,
genre.name as genre, format.name as format
FROM 
book
JOIN author USING(author_id)
JOIN publisher USING(publisher_id)
JOIN format USING(format_id)
JOIN genre USING(genre_id);

-- IMPLICIT JOINS
SELECT 
book.book_id, book.title,
book.num_pages, book.year_published,
book.in_print, book.image, book.price,
author.name as author, author.country as author_country,
publisher.name as publisher, publisher.city as publisher_city,
genre.name as genre, format.name as format
FROM 
book, author, publisher, format, genre
WHERE
book.author_id = author.author_id
AND
book.publisher_id = publisher.publisher_id
AND
book.format_id = format.format_id
AND
book.genre_id = genre.genre_id;

--inner join only returns results common to both tables
SELECT book.title, author.name as author
FROM
book


-- OUTER JOINS
	-- 
SELECT book.title, author.name as author
FROM
book
RIGHT OUTER JOIN author USING(author_id);

-- TO ONLY FONG RECORDS WITHOUT MATCH 
-- USE IS NULL


SELECT book.title, author.name as author_id
FROM
book 
LEFT OUTER JOIN author USING(author_id); 


--CROSS JOIN -- NOT OFTEN USED
-- matches every record in one table with 

-- the keys don't match

SELECT book.title, author.name as author
FROM
book
JOIN author;

--multiply numbers of records in one table with number
-- of records in another table, to determine th enumber
-- of rows in the result...........A CARTESIAN RESULT SET

SELECT MIN(Price) as min_price from book;

SELECT MAX(Price) as max_price from book;

SELECT AVG(Price) as mavg_price from book;

SELECT MIN(Price) as min_price,
MAX(Price) as max_price,
AVG(Price) as mavg_price
 from book;

select publisher.name as publisher,
MIN(Price) as min_price,
MAX(Price) as max_price,
AVG(Price) as mavg_price
from book JOIN publisher using(publisher_id)
GROUP BY publisher;
 +------------------+-----------+-----------+------------+
| publisher        | min_price | max_price | mavg_price |
+------------------+-----------+-----------+------------+
| Ballantine Books |      2.99 |     17.99 |   7.990000 |
| DAW              |     19.99 |     19.99 |  19.990000 |
| Delacorte        |      4.99 |     33.99 |  17.740000 |
| Dell             |      4.99 |     24.99 |  14.990000 |
| Penguin Books    |     22.99 |     22.99 |  22.990000 |
| Putnam           |      3.99 |      3.99 |   3.990000 |
| Sun Press        |     12.99 |     12.99 |  12.990000 |
+------------------+-----------+-----------+------------+