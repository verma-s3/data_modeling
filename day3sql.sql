-- DAY 3 SQL

-- Get a list of authors and books from catalog

--  Sample -- QUESTION  -- GET a list of all authors and books

SELECT 
author, title
FROM 
catalog;


-- ********************************  In CLass Assingment **************************************
-- use exactly for fields in the results;

-- question 1 -- Get a list of books ordered 
-- by year_published with the newset books first 

SELECT 
book_id, title, year_published, in_print 
FROM
catalog
ORDER BY 
year_published desc ;



+---------+-------------------+----------------+----------+
| book_id | title             | year_published | in_print |
+---------+-------------------+----------------+----------+
|       3 | A Day in the Life |           2012 |        1 |
|       4 | Under the Dome    |           2010 |        1 |
|       8 | The Oath          |           2008 |        1 |
|      10 | Flash Forward     |           2006 |        1 |
|       2 | Island            |           2002 |        1 |
|       7 | A Mixed Blessing  |           2002 |        1 |
|       6 | Not a Penny More  |           1980 |        1 |
|       5 | Carpet Baggers    |           1977 |        0 |
|       1 | Dune              |           1975 |        1 |
|       9 | Carrie            |           1975 |        0 |
+---------+-------------------+----------------+----------+


-- Question 2 - get a list of books in which the letters 
-- 'in' appear somewhere in the title.

SELECT 
book_id, title, author, year_published
FROM
catalog
where 
title
LIKE '%in%';

+---------+-------------------+-------------+----------------+
| book_id | title             | author      | year_published |
+---------+-------------------+-------------+----------------+
|       3 | A Day in the Life | Carmen Ynez |           2012 |
|       7 | A Mixed Blessing  | Sally Unger |           2002 |
+---------+-------------------+-------------+----------------+
2 rows in set (0.00 sec)

-- Question 3: get a list of books published between 1971
-- and 2000 inclusive and order the results by the title in 
--  reverse alphabetical order
SELECT 
book_id, title, author, year_published
FROM
catalog
where year_published >= 1971 and year_published <= 2000 
ORDER BY
title desc;


+---------+------------------+-----------------+----------------+
| book_id | title            | author          | year_published |
+---------+------------------+-----------------+----------------+
|       6 | Not a Penny More | Daniel Chambers |           1980 |
|       1 | Dune             | Frank Herbert   |           1975 |
|       9 | Carrie           | Stephen King    |           1975 |
|       5 | Carpet Baggers   | Lee Sheldon     |           1977 |
+---------+------------------+-----------------+----------------+
4 rows in set (0.00 sec)


-- Question 4: get the number of authors in the table 
--in a field called num_authors(only one field in result)

SELECT 
COUNT(DISTINCT author)
AS num_authors 
FROM 
catalog;

+-------------+
| num_authors |
+-------------+
|           9 |
+-------------+
1 row in set (0.04 sec)


-- Quesion 5 - get a list of books where the author name
-- ends in king or chambers

SELECT 
book_id, title, author, year_published
FROM
catalog
where 
author
LIKE '%king' 
OR
author
LIKE '%chambers';

+---------+------------------+-----------------+----------------+
| book_id | title            | author          | year_published |
+---------+------------------+-----------------+----------------+
|       4 | Under the Dome   | Stephen King    |           2010 |
|       6 | Not a Penny More | Daniel Chambers |           1980 |
|       9 | Carrie           | Stephen King    |           1975 |
+---------+------------------+-----------------+----------------+
3 rows in set (0.00 sec)

--- Day 2 Catchup

--LIMIT

-- get a list of books, but only 3 records
-- use a LIMIT clause

SELECT 
book_id, title, author, in_print
FROM catalog
LIMIT 3;

-- LIMIT with offset (menas how many records will be skipped)

SELECT 
book_id, title, author, in_print
FROM catalog
LIMIT 0, 3;

-- year_published >2000 and year_published <2008
-- year_published >=2000 and year_published =<2008

-- ALternative "BETWEEN"

-- list all books published between 1978 and 2000

SELECT 
book_id, title, author, year_published
FROM
catalog
where year_published
BETWEEN 1978 AND 2000;

-- in operator (in -- in_array())

--- find a collection of records where the book_id is one of 3, 7, 9;

SELECT 
book_id, title, author, year_published
FROM
catalog
WHERE 
book_id = 3
OR
book_id = 7
OR
book_id = 9;

-- alternate method
SELECT 
book_id, title, author, year_published
FROM
catalog
WHERE 
book_id IN(3,7,9);

-- A CLOSER LOOK AT AFTER

-- CREATE TABLE WITH THE FOLLOWING fields
-- ID PRIMARY KEY, INTEGER
-- NAME VARCHAR

CREATE TABLE 
test(
id  INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
namr VARCHAR(255)
);

-- alter table and  add a sku field that has max six characters
-- unique
ALTER TABLE test ADD sku CHAR(6) NOT NULL UNIQUE;


-- lets trying adding the primary key
ALTER TABLE test ADD PRIMARY KEY(sku);

-- try dropping the primary key defined
ALTER TABLE test DROP PRIMARY KEY;

 -- 1. remove auto_increment from current primary key
ALTER TABLE test MODIFY id INT NOT NULL;

-- 2. drop th eprimary key (by modifying key (sku))
ALTER TABLE test DROP PRIMARY KEY;

-- 3. add ht enew primary key
ALTER TABLE test ADD PRIMARY KEY(SKU);


-- changing the name of a field with alter table
-- TO rename a field, we must use the change keyword
-- instead of the MODIFY

ALTER TABLE 
test
CHANGE sku -- select a firld to change
product_sku CHAR(6); -- provide a new definition for field

-- how to rename a table 
ALTER TABLE 
test 
RENAME TO test2;

-- create a DUPLICATE TABLE
CREATE TABLE test3 LIKE test2;

-- to copy all data

INSERT INTO test3 
SELECT * FROM test2;


-- 