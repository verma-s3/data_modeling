#day 2 SQL
-- MYSQL Data Types

-- DECIMAL -- fixed place decimal.
ALTER TABLE catalog

ADD price DECIMAL(7,2);

-- ALTER TABLE allows to us to change the structure 
-- of an existing table .. add or remove fields
-- or chnage the definition of the field.

Alter table 
catalog
DROP price;

-- add a field in a specific position
ALter table 
catalog
add
price decimal(7,2)
after genre;

-- add a title field
ALTER TABLE 
catalog
ADD
title VARCHAR(255)
after book_id;

-- add a book
INSERT INTO 
catalog
(title, author, genre, year_published, price)
VALUES
('Dune','Sonia Verma', 'SF','1975','200');

INSERT INTO 
catalog
(title, author, genre, year_published, price)
VALUES
('sports','iqbal', 'SF','1975','200'),
('News','Harsh', 'EF','1945','2450'),
('Horoor','Gurjot', 'QF','1775','300'),
('valientine','nisha', 'DF','1995','4500'),
('love','jiya', 'VF','1977','50'),
('death','daksh', 'ZF','1971','50');

--UPDTAE records with the UPDATE keyword
UPDATE catalog
SET genre = "politics" WHERE book_id = 4;

UPDATE catalog
SET price = "799" WHERE title = 'Dune';

-- change the field price to have a defaut of zero

Alter table 
catalog
MODIFY price
DECIMAL(5,2)
NOT NULL DEFAULT 0;

-- add aecord without price

INSERT Into catalog
(title, author, genre, year_published)
VALUES
('harry','harry potter', 'SF','1975');

--set books even id to false

UPDATE catalog 
SET in_print = false
WHERE book_id % 2 = 0;

Select title, author from catalog where in_print is true;


SELECT title, author , In_print, year_published
from 
catalog
ORDER BY title;


-- test for null
select * from 
tablename where fieldname IS NULL


-- to test for true for any other value 
select * from tablename WHERE fieldname IS true


--RESOLVE to true ONLY for 1
select * 
FROM tablename
Where fieldname = true;

--looking at CHAR and ENUM datatypes

--use people databse
-- add postal_code to the user's table,
--that has a datatype of CHAR and ALLows only 6 characters

--add a gender field to the isers table,
--that has a datatype of enum and allows only 'm'/'f'

ALTER TABLE 
catalog 
RENMAE to catalog_old;





Select title from catalog;
select author from catalog;

-- one way to serch is with the =\
-- another way is to with the like

SELECT title, author 
FROM 
catalog
WHERE
author = 'Stephen King';

-- seaching using like
--like usin gtwo wildcard characters: % _ 
-- % means any number of characters
-- _means ANY SINGLE CHARACTERS
-- either can be used anywhere in a string

Select title, author from catalog where author like '%ing';
Select title, author from catalog where author like 'ing%';
Select title, author from catalog where author like '%i%';


CREATE TABLE users(
  id INTEGER NOT NULL PRIMARY KEY, 
  name VARCHAR(255),
  email VARCHAR(255),
  password VARCHAR(255),
  created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);