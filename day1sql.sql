-- Day 1 SQL

-- some basic DML Commands

-- See a list of existing databases

SHOW DATABASES:


-- TO CREATE nre databases, we use the CREATE DATABASE
CREATE DATABASE people;

-- TO USE A DATABASE: USE databasename;

USE people;

-- show a list of tables;
SHOW TABLES;

-- Create a new table
CREATE TABLE tablename[...];

-- Show HOW a table was created
SHOW CREATE TABLE tablename;

-- and also ........
DESCRIBE tablename;

-- drop a table 
DROP TABLE IF EXISTS users;

-- to see data is exists or not or simlpy describe waht inside it...
DESCRIBE users;


-- Creates a USERS table in the PEOPLE database
CREATE TABLE users(
  user_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
  name VARCHAR (255),
  email VARCHAR(255),
  password VARCHAR(255)
);

-- using ALTER TABLE to modify an existing table 
ALTER TABLE users ADD is_active BOOLEAN NOT NULL DEFAULT true;
ALTER TABLE users ADD created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;

-- ADD REcords to tale using the INSERT Statement
INSERT INTO users 
(name, email, password)
values
('nisha','nisha@gmail.com','mypass'),
('gurjot','gurjot@gmail.com','mypass'),
('harsh','harsh@gmail.com','mypass'),
('iqbal','iqbal@gmail.com','mypass'),
('jiya','jiya@gmail.com','mypass'),
('katie','katie@gmail.com','mypass'),
('krita','krita@gmail.com','mypass');


-- to see the data from the tables
Select * FROM users;

-- Granting privileges on a database

CREATE USER 'web_user'@'localhost'identified by 'mypass';

GRANT ALL
ON people.*
to 'web_user'@'localhost'
identified by 'mypass';

--create a database with a utf default character set.

CREATE DATABASE books DEFAULT CHARSET utf8mb4;


CREATE TABLE catalog(
  book_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
  author VARCHAR(255),
  genre VARCHAR(255),
  year_published INT(4),
  in_print BOOLEAN NOT NULL DEFAULT true,
  created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
)engine=InnoDB;