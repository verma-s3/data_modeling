# question 
# write the query that will generate results
# show result(book_id, title, genre, author, price)
#only show result  where price<=9.99

SELECT 
book.book_id, book.title,book.price,
author.name as author,
genre.name as genre
FROM 
book
JOIN author USING (author_id)
JOIN genre USING (genre_id)
WHERE
book.price <= 9.99;

+---------+------------------+-------+-----------------+----------+
| book_id | title            | price | author          | genre    |
+---------+------------------+-------+-----------------+----------+
|       1 | Dune             |  5.99 | Frank Herbert   | SF       |
|       2 | Island           |  4.99 | Richard Laymon  | Horror   |
|       5 | Carpet Baggers   |  3.99 | Lee Sheldon     | Drama    |
|       6 | Not a Penny More |  5.99 | Daniel Chambers | Politics |
|       9 | Carrie           |  4.99 | Stephen King    | Horror   |
|      12 | Caves of Steel   |  4.99 | Isaac Asimov    | SF       |
|      14 | Dune Messiah     |  2.99 | Frank Herbert   | SF       |
+---------+------------------+-------+-----------------+----------+

## GROUP BY
-- WRITE THE QUERY TO CREATE TH EFOLLOWING RESULT SET:
#RESULT(publisher, num_books, avg_price, max_price, min_price)

SELECT publisher.name as publisher,
COUNT(book.title) as num_books,
AVG(book.price) as avg_price,
MAX(book.price) as max_price,
MIN(book.price) as min_price
FROM 
book 
JOIN publisher using(publisher_id)
GROUP BY publisher;

+------------------+-----------+-----------+-----------+-----------+
| publisher        | num_books | avg_price | max_price | min_price |
+------------------+-----------+-----------+-----------+-----------+
| Ballantine Books |         4 |  7.990000 |     17.99 |      2.99 |
| DAW              |         1 | 19.990000 |     19.99 |     19.99 |
| Delacorte        |         4 | 17.740000 |     33.99 |      4.99 |
| Dell             |         2 | 14.990000 |     24.99 |      4.99 |
| Penguin Books    |         1 | 22.990000 |     22.99 |     22.99 |
| Putnam           |         1 |  3.990000 |      3.99 |      3.99 |
| Sun Press        |         1 | 12.990000 |     12.99 |     12.99 |
+------------------+-----------+-----------+-----------+-----------+


-- Question 3:
# wrie a query to creat ethe folowing result set:
# RESULT(publisher, num_books, avg_price, max_price, min_price)
# but also show th e num_books, avg_price, max_price, and min_price 
# per author by publisher

SELECT publisher.name as publisher,
author.name as author,
COUNT(book.title) as num_books,
AVG(book.price) as avg_price,
MAX(book.price) as max_price,
MIN(book.price) as min_price
FROM 
book 
JOIN publisher using(publisher_id)
JOIN author using(author_id)
GROUP BY publisher,author;

+------------------+------------------+-----------+-----------+-----------+-----------+
| publisher        | author           | num_books | avg_price | max_price | min_price |
+------------------+------------------+-----------+-----------+-----------+-----------+
| Ballantine Books | Frank Herbert    |         2 |  4.490000 |      5.99 |      2.99 |
| Ballantine Books | Stephen King     |         2 | 11.490000 |     17.99 |      4.99 |
| DAW              | Robert Sawyer    |         1 | 19.990000 |     19.99 |     19.99 |
| Delacorte        | Daniel Chambers  |         1 |  5.990000 |      5.99 |      5.99 |
| Delacorte        | Enid Blyton      |         1 | 33.990000 |     33.99 |     33.99 |
| Delacorte        | Isaac Asimov     |         1 |  4.990000 |      4.99 |      4.99 |
| Delacorte        | Michael Connelly |         1 | 25.990000 |     25.99 |     25.99 |
| Dell             | John Lescroart   |         1 | 24.990000 |     24.99 |     24.99 |
| Dell             | Richard Laymon   |         1 |  4.990000 |      4.99 |      4.99 |
| Penguin Books    | Carmen Ynez      |         1 | 22.990000 |     22.99 |     22.99 |
| Putnam           | Lee Sheldon      |         1 |  3.990000 |      3.99 |      3.99 |
| Sun Press        | Sally Unger      |         1 | 12.990000 |     12.99 |     12.99 |
+------------------+------------------+-----------+-----------+-----------+-----------+

-- Question 4:
# wrie a query to creat ethe folowing result set:
# RESULT(publisher, num_books, avg_price, max_price, min_price)
#but only show publisher with less than 4 books

SELECT publisher.name as publisher,
author.name as author,
COUNT(book.title) as num_books,
AVG(book.price) as avg_price,
MAX(book.price) as max_price,
MIN(book.price) as min_price
FROM 
book 
JOIN publisher using(publisher_id)
JOIN author using(author_id)
GROUP BY publisher
HAVING num_books < 4;
+---------------+----------------+-----------+-----------+-----------+-----------+
| publisher     | author         | num_books | avg_price | max_price | min_price |
+---------------+----------------+-----------+-----------+-----------+-----------+
| DAW           | Robert Sawyer  |         1 | 19.990000 |     19.99 |     19.99 |
| Dell          | Richard Laymon |         2 | 14.990000 |     24.99 |      4.99 |
| Penguin Books | Carmen Ynez    |         1 | 22.990000 |     22.99 |     22.99 |
| Putnam        | Lee Sheldon    |         1 |  3.990000 |      3.99 |      3.99 |
| Sun Press     | Sally Unger    |         1 | 12.990000 |     12.99 |     12.99 |
+---------------+----------------+-----------+-----------+-----------+-----------+

--Question 5:
-- show author who have more than 1 book in our database
-- result (author, num_books)

SELECT 
author.name as author,
COUNT(book.title) as num_books
FROM 
book 
JOIN author using(author_id)
GROUP BY author
HAVING num_books >1 ;

+---------------+-----------+
| author        | num_books |
+---------------+-----------+
| Frank Herbert |         2 |
| Stephen King  |         2 |
+---------------+-----------+

--Question 5:
--show all authors and a count of their books, but also show authors have no books

SELECT 
author.name as author,
COUNT(book.title) as num_books
FROM 
book 
right JOIN author using(author_id)
GROUP BY author
order by num_books desc;
+------------------+-----------+
| author           | num_books |
+------------------+-----------+
| Frank Herbert    |         2 |
| Stephen King     |         2 |
| Richard Laymon   |         1 |
| Carmen Ynez      |         1 |
| Lee Sheldon      |         1 |
| Daniel Chambers  |         1 |
| Sally Unger      |         1 |
| John Lescroart   |         1 |
| Robert Sawyer    |         1 |
| Michael Connelly |         1 |
| Isaac Asimov     |         1 |
| Enid Blyton      |         1 |
| Tommy Dougald    |         0 |
| Michael Thompson |         0 |
| Jim Butcher      |         0 |
| Mark Twain       |         0 |
| Brent Weeks      |         0 |
+------------------+-----------+

-- QUESTION 6:
-- show all books, and yourself as the author
-- result (title, author);

SELECT 
title, 'Sonia' as author
FROM
book;

+---------------------+--------+
| title               | author |
+---------------------+--------+
| Dune                | Sonia  |
| Island              | Sonia  |
| A Day in the Life   | Sonia  |
| Under the Dome      | Sonia  |
| Carpet Baggers      | Sonia  |
| Not a Penny More    | Sonia  |
| A Mixed Blessing    | Sonia  |
| The Oath            | Sonia  |
| Carrie              | Sonia  |
| Flash Forward       | Sonia  |
| The Black Box       | Sonia  |
| Caves of Steel      | Sonia  |
| Castle of Adventure | Sonia  |
| Dune Messiah        | Sonia  |
+---------------------+--------+


# add a book to the book database with the following information:
-- title is 'A day in the sun'
-- numver of pages  325
-- year published is 2015
-- price is 20 dollars
-- book is in print
-- a line of lorem ipsum for the description
-- genre is literature
-- author is Stephen King
-- publisher is Ballantine
-- Format is paper

INSERT into book
(title, num_pages, year_published, price, description, in_print, genre_id,author_id, format_id, publisher_id)
Values
('A Day in the SUN', 325, 2015, 20, 'a line of lorem ipsum for the description',1,3,4,1,1);

+---------+---------------------+----------------+-----------+----------+-------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------------+-----------+--------------+-----------+----------+
| book_id | title               | year_published | num_pages | in_print | price | description                                                                                                                                                                                                                                                                                   | image                   | author_id | publisher_id | format_id | genre_id |
+---------+---------------------+----------------+-----------+----------+-------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------------+-----------+--------------+-----------+----------+
|       1 | Dune                |           1975 |       556 |        1 |  5.99 | <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p> | dune.jpg                |         1 |            1 |         1 |        1 |
|       2 | Island              |           2002 |       345 |        1 |  4.99 | <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p> | island.jpg              |         2 |            2 |         1 |        2 |
|       3 | A Day in the Life   |           2012 |       704 |        1 | 22.99 | <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p> | a_day_in_the_life.jpg   |         3 |            3 |         2 |        3 |
|       4 | Under the Dome      |           2010 |      1200 |        0 | 17.99 | <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p> | under_the_dome.jpg      |         4 |            1 |         3 |        2 |
|       5 | Carpet Baggers      |           1977 |       340 |        1 |  3.99 | <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p> | the_carpet_baggers.jpg  |         5 |            4 |         1 |        4 |
|       6 | Not a Penny More    |           1980 |       300 |        1 |  5.99 | <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p> | not_a_penny_more.jpg    |         6 |            5 |         1 |        5 |
|       7 | A Mixed Blessing    |           2002 |       450 |        1 | 12.99 | <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p> | a_mixed_blessing.jpg    |         7 |            6 |         3 |        5 |
|       8 | The Oath            |           2008 |       500 |        0 | 24.99 | <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p> | the_oath.jpg            |         8 |            2 |         2 |        6 |
|       9 | Carrie              |           1975 |       300 |        1 |  4.99 | <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p> | carrie.jpg              |         4 |            1 |         1 |        2 |
|      10 | Flash Forward       |           2006 |       417 |        1 | 19.99 | <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p> | flash_forward.jpg       |         9 |            7 |         2 |        1 |
|      11 | The Black Box       |           2012 |       345 |        1 | 25.99 | <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p> | black_box.jpg           |        21 |            5 |         2 |        3 |
|      12 | Caves of Steel      |           1957 |       198 |        1 |  4.99 | <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p> | caves_of_steel.jpg      |        20 |            5 |         2 |        1 |
|      13 | Castle of Adventure |           1944 |       224 |        1 | 33.99 | <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p> | castle_of_adventure.jpg |        22 |            5 |         2 |        1 |
|      14 | Dune Messiah        |           1977 |       350 |        1 |  2.99 | <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p> | dune_messiah.jpg        |         1 |            1 |         1 |        1 |
|      15 | A Day in the SUN    |           2015 |       325 |        1 | 20.00 | a line of lorem ipsum for the description                                                                                                                                                                                                                                                     | NULL                    |         4 |            1 |         1 |        3 |
+---------+---------------------+----------------+-----------+----------+-------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------------+-----------+--------------+-----------+----------+
15 rows in set (0.00 sec)

